# Use a base image (e.g., Python with Playwright pre-installed)
FROM mcr.microsoft.com/playwright/python:v1.37.0-jammy

# Set the working directory
WORKDIR /app

# Install python3-venv package and apt-utils
RUN apt-get update && \
    apt-get install -y python3-venv apt-utils && \
    apt-get install -y xvfb && \
    apt-get install -y x11-xkb-utils && \
    apt-get install -y xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic && \
    rm -rf /var/lib/apt/lists/*

# Create a virtual environment and activate it
RUN python -m venv venv
SHELL ["/bin/bash", "-c"]
RUN source venv/bin/activate

# Install any needed packages specified in requirements.txt
COPY requirements.txt /app/
RUN pip install -r requirements.txt
RUN playwright install

# Create directories for results and screenshots
RUN mkdir /app/results
RUN mkdir /app/screenshots

# Copy the test code and data into the container
COPY . /app

# Set the environment variable for screenshot directory
ENV DOCKER_SCREENS_DIR=/app/screenshots/
ENV PYTHONPATH /app:$PYTHONPATH

# Set up Xvfb
ENV DISPLAY=:99
RUN Xvfb $DISPLAY -screen 1 1920x1080x24 &

# Create an entrypoint script
COPY entrypoint.sh /app/
RUN chmod +x /app/entrypoint.sh

# Run pytest
ENTRYPOINT ["/app/entrypoint.sh"]
