import logging
import re
from common.page import Page

import utils.logger_utils as lu
from locators.rozetka_home_page_locators import RozetkaHomePageLocators
from locators.rozetka_search_page_locators import RozetkaSearchResultsPageLocators


class RozetkaSearchResultsPage(Page):
    log = lu.custom_logger(logging.DEBUG)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.locators = RozetkaSearchResultsPageLocators

    def get_quantity_value(self):
        value = self.locate_element(self.locators.TOTAL_PRODUCTS_QUANTITY.xpath)
        return extract_integer_from_string(value.text_content())

    def get_summarized_quantity_value(self):
        elements = self.locate_elements(self.locators.SUMMARIZED_PRODUCTS_QUANTITIES.xpath)
        strings = self.get_elements_text(elements)

        integer_values = [extract_integer_from_string(string) for string in strings]

        total_sum = sum(integer_values)

        return total_sum


def extract_integer_from_string(input_string):
    # Define a regular expression pattern to match integers enclosed in parentheses
    pattern = r'\((\d+)\)'

    # Use re.search to find the first match in the input_string
    match = re.search(pattern, input_string)

    # If a match is found, extract and return the integer value
    if match:
        integer_value = int(match.group(1))
        return integer_value
    else:
        # If no match is found, return None or raise an exception, depending on your requirements
        return 0
