import logging

from common.ABC import ABC
from utils.logger_utils import custom_logger

logger = custom_logger(log_level=logging.DEBUG)


class Test(ABC):

    def _log_and_raise_error(self, message, e):
        logger.error(message)
        raise AssertionError(e)

    def _assert(self, condition, message, expected_value, success_message):
        try:
            assert condition, expected_value
            logger.info(success_message)
        except AssertionError as e:
            raise AssertionError(f"Assertion Failed: {message or ''}{e}")

    def assert_equal(self, actual, expected, message=None):
        self._assert(actual == expected, message, f"Expected {expected}, but got {actual}",
                     f"Expected '{expected}' and got '{actual}'")

    def assert_not_equal(self, actual, not_expected, message=None):
        self._assert(actual != not_expected, message, f"Did not expect {not_expected}, but got {actual}",
                     f"Did not expect '{not_expected}' and got '{actual}'")

    def assert_all_elements_equal(self, elements, expected_element, message=None):
        result = all(element == expected_element for element in elements)
        self._assert(result, message, f"All elements do not match '{expected_element}'",
                     f"All elements match '{expected_element}'")

    def assert_all_elements_dont_equal(self, elements, not_expected_element, message=None):
        result = all(element != not_expected_element for element in elements)
        self._assert(result, message, f"Some elements equal '{not_expected_element}'",
                     f"None of the elements equal '{not_expected_element}'")

    def assert_true(self, condition, message=None):
        self._assert(condition is True, message, f"Expected True, but got {condition}", "Condition is True")

    def assert_false(self, condition, message=None):
        self._assert(condition is False, message, f"Expected False, but got {condition}", "Condition is False")
