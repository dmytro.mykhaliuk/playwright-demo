#!/bin/bash
# Start Xvfb
Xvfb :99 -screen 0 1920x1080x24 &
export DISPLAY=:99

# Activate virtual environment
source venv/bin/activate

# Run pytest
pytest
