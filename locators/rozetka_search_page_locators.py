from enum import Enum


class RozetkaSearchResultsPageLocators(Enum):

    @property
    def css(self):
        return self.value['css']

    @property
    def xpath(self):
        return self.value['xpath']

    TOTAL_PRODUCTS_QUANTITY = {
        'css': '',
        'xpath': '//span[@class="sidebar-block__quantity"]'
    }

    SUMMARIZED_PRODUCTS_QUANTITIES = {
        'css': '',
        'xpath': '//span[@class="sidebar-block__quantity ng-star-inserted"]'
    }
